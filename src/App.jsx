import React, { useState } from "react";
import "./App.css";
import Header from "./components/Header";
import { RouterProvider } from "react-router-dom";
import routerFunction from "./router.jsx";

export const ThemeContext = React.createContext();

function App() {
  const router = routerFunction();
  const [dark, setDark] = useState(false);

  return (
    <div className={dark ? "darker" : null}>
      <ThemeContext.Provider value={{ dark, setDark }}>
        <Header />
        <RouterProvider router={router} />
      </ThemeContext.Provider>
    </div>
  );
}

export default App;
