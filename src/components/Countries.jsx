import React, { useContext, useEffect, useState } from "react";
import SearchBar from "./SearchBar";
import "../style/countries.css";
import Filter from "./Filter";
import { ThemeContext } from "../App";
import Sort from "./Sort";
import { Link } from "react-router-dom";
import { Rolling } from "react-loading-io";
import CurrencyFilter from "./CurrencyFilter";

export default function Countries() {
  const [countryData, setCountryData] = useState([]);
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [currency, setCurrency] = useState("");
  const [name, setName] = useState("");
  const [loader, setLoader] = useState(true);
  const [sort, setSort] = useState("");
  const [error, setError] = useState(false);

  const { dark } = useContext(ThemeContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch("https://restcountries.com/v3.1/all");
        const res2 = await res.json();
        const dataReturned = res2;
        setLoader(false);
        setCountryData(dataReturned);
      } catch (error) {
        setLoader(false);
        setError(true);
      }
    };

    fetchData();
  }, []);

  

  const filterCountries = [];

  countryData.forEach((country) => {
    let currencies = [];
    if (country.currencies) {
      let currencyArray = Object.values(country.currencies);
      currencies = currencyArray.map((curr) => {
        return curr.name;
      });
    }
    if (
      (!region || country.region === region) &&
      (!subRegion || country.subregion === subRegion) &&
      (!currency || currencies.includes(currency)) &&
      (!name || country.name.common.toLowerCase().includes(name.toLowerCase()))
    ) {
      filterCountries.push(country);
    }
  });
  if (sort) {
    if (sort.includes("population")) {
      filterCountries.sort((countryA, countryB) => {
        return countryA.population - countryB.population;
      });
    } else {
      filterCountries.sort((countryA, countryB) => {
        return countryA.area - countryB.area;
      });
    }
    if (sort.includes("Desc")) {
      filterCountries.reverse();
    }
  }

  return (
    <>
      <div className="functions">
        <SearchBar onSearch={setName} />
        <Sort changeSort={setSort} />
        <CurrencyFilter countryData={countryData} setCurrency={setCurrency} />
        <Filter
          countryData={countryData}
          type={"region"}
          setRegion={setRegion}
          setSubRegion={setSubRegion}
          subRegion={subRegion}
          region={region}
        />
        <Filter
          countryData={countryData}
          type={region}
          setRegion={setRegion}
          setSubRegion={setSubRegion}
          subRegion={subRegion}
          region={region}
        />
      </div>
      <div className={dark ? "darker box" : "box"}>
        <div className="content">
          {error && <p>Error</p>}
          {loader && <Rolling size={100} />}
          {!loader && !error && filterCountries.length === 0 && (
            <p>No countries found.</p>
          )}
          {!loader &&
            !error &&
            filterCountries.length > 0 &&
            filterCountries.map((country) => (
              <Link to={`country/${country.cca3}`} key={country.cca3}>
                <div
                  key={country.cca3}
                  className={dark ? "dark container" : "container"}
                >
                  <img src={country.flags.png} alt={country.name.common} />
                  <div className={dark ? "info dark" : "info"}>
                    <h3>{country.name.common}</h3>
                    <div>
                      <p>
                        Population: <span>{country.population}</span>
                      </p>
                      <p>
                        Region: <span>{country.region}</span>
                      </p>
                      <p>
                        Capital:{" "}
                        <span>
                          {country.capital ? country.capital[0] : "No Capital"}
                        </span>
                      </p>
                      <p>
                        Currency:{" "}
                        <span>
                          {country.currencies
                            ? Object.keys(country.currencies)
                            : "No Currency"}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </Link>
            ))}
        </div>
      </div>
    </>
  );
}
