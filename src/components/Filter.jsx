import { useContext } from "react";
import { ThemeContext } from "../App";
import "../style/filter.css";

export default function Filter({
  countryData,
  type,
  setRegion,
  setSubRegion,
  subRegion,
  region,
}) {
  const { dark } = useContext(ThemeContext);

  function handleChange(e) {
    if (type === "region") {
      console.log("HIII");
      setSubRegion("");
      setRegion(e.target.value);
    } else {
      console.log("hey");
      setSubRegion(e.target.value);
    }
  }

  const regions = countryData.reduce((acc, country) => {
    if (type !== "region") {
      if (!acc.includes(country["subregion"]) && country["region"] === type) {
        acc.push(country["subregion"]);
      }
    } else {
      if (!acc.includes(country["region"])) {
        acc.push(country["region"]);
      }
    }
    return acc;
  }, []);

  return (
    <>
      <div className="filter">
        <select
          value={type != "region" ? subRegion : region}
          className={dark ? "dark" : null}
          name={type === "region" ? "region" : "subregion"}
          id={type === "region" ? "region" : "subregion"}
          onChange={handleChange}
        >
          <option>
            Filter by {type === "region" ? "region" : "subregion"}
          </option>
          {regions.map((region, index) => {
            return (
              <option key={index} value={region}>
                {region}
              </option>
            );
          })}
        </select>
      </div>
    </>
  );
}
