import { useContext } from "react";
import "../style/sort.css";
import { ThemeContext } from "../App";

export default function Sort({ changeSort }) {
  const { dark } = useContext(ThemeContext);

  function handleChange(e) {
    changeSort(e.target.value)
  }

  return (
    <>
      <select name="sort" id="sort" className={dark ? "dark" : null} onChange={handleChange}>
        <option value="">Sort</option>
        <optgroup label="Population">
          <option value="populationAsc">Ascending</option>
          <option value="populationDesc">Descending</option>
        </optgroup>
        <optgroup label="Area">
          <option value="areaAsc">Ascending</option>
          <option value="areaDesc">Descending</option>
        </optgroup>
      </select>
    </>
  );
}
