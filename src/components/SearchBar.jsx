import React,{useContext} from "react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../style/searchbar.css";
import { ThemeContext } from "../App";

export default function SearchBar({onSearch }) {
  
  const { dark } = useContext(ThemeContext);

  function handleChange(event){
    onSearch(event.target.value);
  };

  return (
    <div className={dark ? "dark searchBar" : "searchBar"}>
      <FontAwesomeIcon icon={faSearch} />
      <input
        type="text"
        className={dark ? "dark" : null}
        placeholder="Search for a country..."
        onChange={handleChange}
      />
    </div>
  );
}
