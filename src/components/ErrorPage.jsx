export default function ErrorPage({ error }) {
    console.log(error);
    return (
      <div>
        <h1>Error</h1>
        <p>Sorry, there was a problem loading the page.</p>
        <p>Error message: {error}</p>
      </div>
    );
  }
  