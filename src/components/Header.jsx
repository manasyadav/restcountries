import '../style/header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon } from '@fortawesome/free-regular-svg-icons';
import { ThemeContext} from '../App';
import { useContext } from 'react';

export default function Header() {

    const {dark, setDark} = useContext(ThemeContext);
    function handleCheck(event){
        if(event.target.checked){
            console.log('Checked');
            setDark(true);
            
        }
        else{
            setDark(false);
        }
    }

    return(
        <header className={dark ? 'dark' : null}>
            <h1>Where in the world?</h1>
            <input type="checkbox" name="darkmode" id="darkmode" onChange={handleCheck} hidden/> 
            <label htmlFor="darkmode"><FontAwesomeIcon icon={faMoon} /> {dark ? 'Light Mode' : 'Dark Mode'}</label>
        </header>
    );
}