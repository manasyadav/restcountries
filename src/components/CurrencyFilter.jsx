import { useContext } from "react";
import { ThemeContext } from "../App";

export default function CurrencyFilter({ countryData, setCurrency }) {

    const {dark} = useContext(ThemeContext);

    function handleSelect(e) {
        setCurrency(e.target.value);
    }

    let currencies = countryData.reduce((accumulator, country) => {
    if (country.currencies) {
      let currencyArray = Object.values(country.currencies);
      currencyArray.map((currency) => {
        if (!accumulator.includes(currency)) {
          accumulator.push(currency.name);
        }
      });
    }
    return accumulator;
  }, []);

  let crr = [...new Set(currencies)]

  return (
    <>
      <div className="filter">
        <select name="currency-filter" onChange={handleSelect} className={dark ? "dark" : null}>
          <option>Currency Filter</option>
          {crr.map((currency, index) => {
            return (
              <option key={index} value={currency}>
                {currency}
              </option>
            );
          })}
        </select>
      </div>
    </>
  );
}
