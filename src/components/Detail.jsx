import { useLoaderData } from "react-router-dom";
import { Link } from "react-router-dom";
import "../style/detail.css";
import { ThemeContext } from "../App";
import { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

export default function Details() {
  const { dark } = useContext(ThemeContext);

  let country = useLoaderData();

  let nativeName = "No Native Name";

  if (country[0].name.nativeName) {
    const nativeNameList = Object.keys(country[0].name.nativeName);

    if (nativeNameList.includes("eng")) {
      nativeName = country[0].name.nativeName.eng.common;
    } else {
      const lastLang = nativeNameList.pop();
      nativeName = country[0].name.nativeName[lastLang].common;
    }
  }

  let currency = "No Currency";
  if (country[0].currencies) {
    currency = Object.values(country[0].currencies)[0].name;
  }

  let languages = "";
  if (country[0].languages) {
    languages = Object.values(country[0].languages);
  }

  let borderCountries = country[0].borders;

  return (
    <>
      <div className="detail-container">
        <div>
          <Link to="/">
            <button className={dark ? "btn dark" : "btn"}>
              <FontAwesomeIcon icon={faArrowLeft} /> Back
            </button>
          </Link>
        </div>
        <div className="details">
          <div>
            <img
              className="big-flag"
              src={country[0].flags.svg}
              alt={country[0].flags.alt}
            />
          </div>
          <div className="country-info">
            <h1>{country[0].name.common}</h1>
            <div>
              <div>
                <p>
                  Native Name: <span>{nativeName}</span>
                </p>
                <p>
                  Population: <span>{country[0].population}</span>
                </p>
                <p>
                  Region: <span>{country[0].region}</span>
                </p>
                <p>
                  Sub Region:{" "}
                  <span>
                    {country[0].subregion
                      ? country[0].subregion
                      : "No Sub Region"}
                  </span>
                </p>
                <p>
                  Capital:{" "}
                  <span>
                    {country[0].capital ? country[0].capital : "No Capital"}
                  </span>
                </p>
              </div>
              <div>
                <p>
                  Top Level Domain:{" "}
                  <span>
                    {country[0].tld ? country[0].tld : "No Top Level Domain"}
                  </span>
                </p>
                <p>
                  Currencies: <span>{currency}</span>
                </p>
                <p>
                  Languages: <span>{languages ? languages.join(",") : "No Languages"}</span>
                </p>
              </div>
            </div>
            <div className="border">
              <p> Border Countries: </p>
              {borderCountries ? (
                borderCountries.map((country) => {
                  return (
                    <Link key={country} to={`/country/${country}`}>
                      <button className={dark ? "btn dark" : "btn"}>
                        {country}
                      </button>
                    </Link>
                  );
                })
              ) : (
                <p>No Border Countries</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
